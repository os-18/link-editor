link-editor is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

---

## link-editor

link-editor is an editor of symbolic links.
This utility can change paths in links, it has options for replacing part of the path.

---

## Ready-made packages

See [download page](https://gitlab.com/tech.vindex/link-editor/wikis/Download-page).

---

## Build from source

### Preparing

Before assembling, you need to install a compiler for D (this project supports [dmd](https://dlang.org/download.html#dmd), [gdc](https://gdcproject.org/) and [ldc](https://github.com/ldc-developers/ldc/)), [chrpath](https://directory.fsf.org/wiki/Chrpath), lsb-release, bash, make. Typically, lsb-release and bash with make are already installed on the system.

For example, in Debian-based distributions, you can install required packages as follows:

```
# apt install ldc chrpath
```

Similarly, in Fedora:

```
# dnf install ldc chrpath
```


### Compilation and installation

Creating of executable bin-file:

```
$ make
```

Also, you can choose a compiler for assembling:

```
$ make DC=gdc
```

Installation (by default, main directory is /usr/local/):

```
# make install
```

After that, the application is ready for use.

You can install this application in any other directory:

```
$ make install PREFIX=/home/$USER/sandbox
```

Uninstall:

```
# make uninstall
```

If you installed in an alternate directory:

```
$ make uninstall PREFIX=/home/$USER/sandbox
```

---

Starting with version 0.5, you can choose an alternative programming language for the project. The symbolic link editor now has two implementations: on *D* and on *Python*. Examples of usage:

```
$ make PLANG=Python
$ make test PLANG=Python
```

This feature is experimental. D is the default programming language for this project.

Starting with version 0.7.5, a Bash-based version is included in the project.

```
$ make PLANG=Bash
$ make test PLANG=Bash
```

---

## Samples of using

The link for gcc (for example, pointing to gcc-7) will point to gcc-8:

```
$ link-editor -l /usr/bin/gcc -p /usr/bin/gcc-8
```

The program can do multiple replacement of the path fragment.

Suppose, in the /mnt/All/ directory, there are links to various
images from the /mnt/Photo/ (for example, from /mnt/Photo/Summer-2010,
/mnt/Photo/My_Photos, etc.). And suddenly you decided to move the
сontent of the directory /mnt/Photo to your home directory (to
"Pictures"). You can fix the links like this:

```
$ link-editor -l /mnt/All/*.jpg -f /mnt/Photo/ -r /home/$USER/Pictures/
```
---

Details on use:

`link-editor --help`

---

## Donate:

* [Bank cards / Yandex.Money Wallets](https://money.yandex.ru/to/410012626193056)

* [Bank cards / Qiwi](https://my.qiwi.com/form/Evgenyi-SUTTDFH0sl)

* [PayPal](https://www.paypal.me/evstulin)

* ETH: `0xA65DEE0Cd4Cd7ef05583a07a4303e0C5B306eA7B`

* BTC: `13uF1MMxWGDktpe8f19rxoDNYkaZ6aoiuU`

---

## Feedback

Questions, suggestions, comments, bugs:

**tech.vindex@gmail.com**

Also use the repository service tools.

---
