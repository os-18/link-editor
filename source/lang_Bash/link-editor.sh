#!/bin/bash -e

# Copyright (C) 2020 Eugene 'Vindex' Stulin
#
# link-editor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

APP_NAME=link-editor
APP_VERSION=0.7.7

if [[ -z $LANG ]]; then
    APP_LANG=en_US
else
    APP_LANG=${LANG%.*}
fi

DATA_DIR=/usr/share
HELP_DIR=${DATA_DIR}/help

HELP_FILE=${HELP_DIR}/${APP_LANG}/${APP_NAME}/help.txt

declare -A translations

translations[0,en_US]="Wrong using of link-editor."
translations[0,ru_RU]="Неправильное использование link-editor."
translations[0,eo]="Neregula uzo de link-editor."

translations[1,en_US]=": this path doesn't exist."
translations[1,ru_RU]=": этот путь не существует."
translations[1,eo]=": ĉi tiu vojo ne ekzistas."

translations[2,en_US]="Do you want to create it? y/n >: "
translations[2,ru_RU]="Вы хотите её создать? д/н >: "
translations[2,eo]="Ĉu vi volas ekzistigi ĝin? j/n >: "

translations[3,en_US]="Do you want to create a broken link? y/n >: "
translations[3,ru_RU]="Вы хотите создать битую ссылку? д/н >: "
translations[3,eo]="Ĉu vi volas krei rompitan ligilon? j/n >: "

translations[4,en_US]=": edited link doesn't exist."
translations[4,ru_RU]=": редактируемая ссылка не существует."
translations[4,eo]=": redaktata ligilo ne ekzistas."

translations[5,en_US]=": link doesn't exist."
translations[5,ru_RU]=": ссылка не существует."
translations[5,eo]=": ligilo ne ekzistas."

translations[6,en_US]=": directory doesn't exist."
translations[6,ru_RU]=": директория не существует."
translations[6,eo]=": dosierujo ne ekzistas."

translations[7,en_US]=": it is not symbolic link."
translations[7,ru_RU]=": это не символическая ссылка"
translations[7,eo]=": ĝi ne estas simbola ligilo"

translations[8,en_US]=": it is not directory."
translations[8,ru_RU]=": это не директория."
translations[8,eo]=": ĝi ne estas dosierujo."

translations[9,en_US]="Skipped."
translations[9,ru_RU]="Пропускаем."
translations[9,eo]="Preterlasas."

translations[10,en_US]="See: link-editor --help"
translations[10,ru_RU]="Смотрите: link-editor --help"
translations[10,eo]="Rigardu: link-editor --help"

translations[11,en_US]="yes"
translations[11,ru_RU]="да"
translations[11,eo]="jes"

translations[12,en_US]="y"
translations[12,ru_RU]="д"
translations[12,eo]="j"

translations[13,en_US]="no"
translations[13,ru_RU]="нет"
translations[13,eo]="ne"

translations[14,en_US]="n"
translations[14,ru_RU]="н"
translations[14,eo]="n"

show_string() {
    local english_string="$1"
    local echo_option="$2"
    if [[ $APP_LANG == "en_US" ]]; then
        echo ${echo_option} "$english_string"
    else
        for ((i=0; i < 16; i++))
        do
            if [[ "${translations[$i,en_US]}" == "$english_string" ]]
            then
                echo ${echo_option} "${translations[$i,$APP_LANG]}"
                return 0
            fi
        done
    fi
}


get_positive_answer() {
    local answer
    read answer
    if [[ "$answer" == "y" || "$answer" == "yes" ]]; then
        return 0
    else
        tr_y=`show_string y`
        tr_yes=`show_string yes`
        if [[ "$answer" == "$tr_y" || "$answer" == "$tr_yes" ]]; then
            return 0
        fi
    fi
    return 1
}


change_symlink() {
    local link="$1"
    local purpose="$2"
    rm -f "$link"
    ln -s "$purpose" "$link"
}


update_symlinks() {
    local purpose="$1"
    local yes_mode="$2"
    local links=( "${@:3}" )
    if [[ ! -e "$purpose" && "$yes_mode" == FALSE ]]; then
        echo -n "$purpose"
        show_string ": this path doesn't exist."
        show_string "Do you want to create a broken link? y/n >: " -n
        get_positive_answer
        if [[ $? -eq 1 ]]; then
            return
        fi
    fi
    for link in "${links[@]}"
    do
        if [[ ! -e "$link" && "$yes_mode" == FALSE ]]; then
            echo -n "$link"
            show_string ": edited link doesn't exist."
            show_string "Do you want to create it? y/n >: "
            get_positive_answer
            [[ $? -eq 1 ]] && continue
        fi
        change_symlink "$link" "$purpose"
    done
}


get_range_by_option() {
    local opt="${1}"
    local -i index=2
    local -i i_start=-1
    local -i i_finish=-1
    for arg in "${@:2}"
    do
        let "index+=1"
        if [[ $i_start -eq -1 ]]; then
            if [[ "$arg" == "$opt"  ]]; then
                let "i_start=index"
                continue
            fi
        elif [[ "$arg" == -* ]]; then
            let "i_finish=index-1"
            break
        fi
    done
    if [[ $i_start -eq -1 ]]; then
        unset OPT_VALUES
        return 0
    elif [[ $i_finish -eq -1 ]]; then
        let "i_finish=$#+1"
    fi
    local number_of_elements
    let "number_of_elements=i_finish-i_start"
    index=0
    unset OPT_VALUES
    for arg in "${@:i_start:number_of_elements}"
    do
        OPT_VALUES[$index]="$arg"
        let "index+=1"
    done
}

main() {
    local -a args=("${@}")
    if [[ $1 == "--version" && $# -eq 1 ]]; then
        echo "${APP_VERSION}"
        exit 0
    elif [[ $1 == "--help" && $# -eq 1 ]]; then
        cat "${HELP_FILE}"
        exit 0
    fi

    if [[ $# -eq 0 ]]; then
        show_string "Wrong using of link-editor."
        show_string "See: link-editor --help"
        exit 9
    fi

    get_range_by_option "-p" "${args[@]}"
    PURPOSE="${OPT_VALUES[0]}"
    get_range_by_option "-l" "${args[@]}"
    LINKS=( "${OPT_VALUES[@]}" )

    # New Target
    if [[ -n "$PURPOSE" && -n "${LINKS[0]}" ]]; then
        local yes_mode=FALSE
        [[ "${args[-1]}" == "-y" || "${args[-1]}" == "--yes" ]] && yes_mode=TRUE
        
        declare -i n_args=2  # for -l and -p
        let "n_args+=${#PURPOSE[@]}"
        let "n_args+=${#LINKS[@]}"
        [[ $yes_mode == TRUE ]] && let "n_args+=1"
        if [[ ${n_args} -ne ${#args[@]} ]]; then
            show_string "Wrong using of link-editor."
            show_string "See: link-editor --help"
            exit 9
        fi

        update_symlinks "$PURPOSE" "$yes_mode" ${LINKS[@]}
        exit 0
    fi

    get_range_by_option "-f" "${args[@]}"
    F_STR="${OPT_VALUES[0]}"
    get_range_by_option "-r" "${args[@]}"
    R_STR="${OPT_VALUES[0]}"

    # Find & Replace
    if [[ -n "${LINKS[0]}" && -n "$F_STR" && -n "$R_STR" ]]; then
        declare -i n_args=3  # for -l, -f, -r
        let "n_args+=${#LINKS[@]}"
        let "n_args+=2"  # for value for '-f' and '-r'
        if [[ ${n_args} -ne ${#args[@]} ]]; then
            show_string "Wrong using of link-editor."
            show_string "See: link-editor --help"
            exit 9
        fi
        for link in "${LINKS[@]}"
        do
            if [[ ! -e "$link" ]]; then
                echo -n "$link"
                show_string ": link doesn't exist." -n
                echo -n " "
                show_string "Skipped."
                continue
            fi
            local path=`readlink "$link"`
            local new_path=${path/$F_STR/$R_STR}
            change_symlink "$link" "$new_path"
        done
        exit 0
    fi

    get_range_by_option "-d" "${args[@]}"
    DIRS=( "${OPT_VALUES[@]}" )

    # Find & Replace in A Directory
    if [[ -n "${DIRS[0]}" && -n "$F_STR" && -n "$R_STR" ]]; then
        depth_param=""
        if [[ "${args[-1]}" != "-R" ]]; then
            depth_param="-maxdepth 1"
        fi

        for dir in "${DIRS[@]}"
        do
            orig_dir="$dir"
            if [[ -L "$dir" ]]; then
                dir=`readlink "$dir"`
            fi
            if [[ ! -d "$dir" ]]; then
                echo -n "$orig_dir"
                show_string ": directory doesn't exist. Skipped." -n
                continue
            fi
            find "$dir" $depth_param -type l | while read line
            do
                local path=`readlink "$line"`
                local new_path="${path/$F_STR/$R_STR}"
                [[ "$path" == "$new_path" ]] && continue
                change_symlink "$line" "$new_path"
            done
        done
        exit 0
    fi
    show_string "Wrong using of link-editor."
    show_string "See: link-editor --help"
    exit 9
}
main "${@}"
