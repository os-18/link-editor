/*
 * Copyright (C) 2018-2020 Eugene 'Vindex' Stulin
 *
 * link-editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import std.algorithm,
       std.array,
       std.exception,
       std.getopt,
       std.process,
       std.file,
       std.path,
       std.stdio,
       std.string,
       std.parallelism;

import additional_functions;


immutable app = "link-editor";
immutable appVersion = import("version").strip;
immutable translations = mixin(import("translations.dtxt").strip);
immutable string helpDir;

shared static this() {
    auto appDir = readLink("/proc/self/exe").dirName;
    auto shareDir = buildNormalizedPath(appDir, "..", "share");
    helpDir = shareDir ~ "/help/";
}

/**
 * Mode of application work
 */
enum AppMode {
    versionDisplay,
    helpDisplay,
    setting,
    simpleReplacing,
    replacingInDirectory
}


int main(string[] args) {
    initLocalization(translations);
    immutable wrongUsing = "Wrong using of link-editor."._s;

    if (args.length == 1) {
        stderr.writeln(wrongUsing);
        stderr.writeln("See: link-editor --help"._s);
        return 9;
    }
    
    string[] links;
    string[] dirs;
    string pathForSetting;
    bool yesMode;
    string substringForFinding;
    string replacementString;
    bool recursively;

    AppMode appMode;
    if (args[1] == "--help") {
        appMode = AppMode.helpDisplay;
        args = args.remove(1);
    } else if (args[1] == "--version") {
        appMode = AppMode.versionDisplay;
        args = args.remove(1);
    } else {
        links = args.extractRangeForFlag("-l");
        pathForSetting = args.extractValueForFlag("-p");
        if (!pathForSetting.empty) { //setting new link path
            appMode = AppMode.setting;
            getopt(args, config.passThrough, "y|yes", &yesMode);
        } else { //replacing
            substringForFinding = args.extractValueForFlag("-f");
            replacementString = args.extractValueForFlag("-r");
            if (!links.empty) {
                appMode = AppMode.simpleReplacing;
            } else {
                appMode = AppMode.replacingInDirectory;
                dirs = args.extractRangeForFlag("-d");
                getopt(args, config.passThrough, "R", &recursively);
            }
        }
    }
    

    if (args.length != 1) {
        stderr.writeln(wrongUsing);
        stderr.writeln("See: link-editor --help"._s);
        return 9;
    }

    final switch(appMode) {
        case AppMode.helpDisplay:
            showHelp();
            break;
        case AppMode.versionDisplay:
            showVersion();
            break;
        case AppMode.setting:
            setLinks(links, pathForSetting, yesMode);
            break;
        case AppMode.simpleReplacing:
            replacePartsOfLinks(links, substringForFinding, replacementString);
            break;
        case AppMode.replacingInDirectory:
            replacePartsOfLinksInDir(dirs,
                                     substringForFinding,
                                     replacementString,
                                     recursively);
            break;
    }
    
    return 0;
}


void showHelp() {
    immutable lang = getCurrentLanguage();
    string pathToHelpFile = helpDir ~ lang ~ "/" ~ app ~ "/help.txt";
    if (!exists(pathToHelpFile)) {
        pathToHelpFile = helpDir ~ "en_US/" ~ app ~ "/help.txt";
    }
    writeln(readText(pathToHelpFile));
}


void showVersion() {
    writeln(appVersion);
}


void setLinks(string[] links, string pathForSetting, bool yesMode) {
    if (!pathForSetting.exists && !yesMode) {
        writeln(pathForSetting, ": this path doesn't exist."._s);
        write("Do you want to create a broken link? y/n >: "._s);
        if (false == getPositiveAnswer()) return;
    }
    foreach(l; links) {
        if (!l.exists() && !yesMode) {
            writeln(l, ": edited link doesn't exist."._s);
            write("Do you want to create it? y/n >: "._s);
            if (false == getPositiveAnswer()) continue;
        }
        if (l.exists) {
            std.file.remove(l);
        }
        symlink(pathForSetting, l);
    }
}


bool checkDir(string dirpath) {
    if (!dirpath.exists()) {
        stderr.write(dirpath, ": directory doesn't exist."._s);
        return false;
    } else if (!dirpath.isDir) {
        stderr.write(dirpath, ": it is not directory."._s);
        return false;
    }
    return true;
}


bool checkSymlink(string link) {
    if (!link.exists()) {
        stderr.write(link, ": link doesn't exist."._s);
        return false;
    } else if (!link.isSymlink) {
        stderr.write(link, ": it is not symbolic link."._s);
        return false;
    }
    return true;
}


void replacePathPart(string l,
                     string substringForFinding,
                     string replacementString) {
    string curAddress = readLink(l);
    string newAddress = curAddress.replace(substringForFinding,
                                           replacementString);
    if (curAddress != newAddress) {
        std.file.remove(l);
        symlink(newAddress, l);
    }
}


void replacePartsOfLinksInDir(string[] dirs,
                              string substringForFinding,
                              string replacementString,
                              bool recursively = false)
body {
    void handleSymlinks(string catalog) {
        catalog = getAbsolutePath(catalog);
        auto entries = std.file.dirEntries(catalog, SpanMode.shallow).array;
        foreach(entry; parallel(entries)) {
            if (entry.isSymlink) {
                replacePathPart(entry.name,
                                substringForFinding,
                                replacementString);
            } else if (recursively && entry.isDir) {
                handleSymlinks(entry.name);
            }
        }
    }
    foreach(d; dirs) {
        if (!checkDir(d)) {
            stderr.writeln(" ", "Skipped."._s);
            continue;
        }
        d = d.isSymlink ? readLink(d) : d;
        handleSymlinks(d);
    }
}


void replacePartsOfLinks(string[] links,
                         string substringForFinding,
                         string replacementString)
body {
    foreach(l; links) {
        if (!checkSymlink(l)) {
            stderr.writeln(" ", "Skipped."._s);
            continue;
        }
        replacePathPart(l, substringForFinding, replacementString);
    }
}


bool getPositiveAnswer() {
    string answer = readln;
    answer = toLower(std.string.strip(answer));
    if (answer.among("y"._s, "yes"._s, "y", "yes", "")) {
        return true;
    }
    return false;
}
