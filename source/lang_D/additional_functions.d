/* This file is part of link-editor and it contains
 * a part of functional from the Amalthea library.
 * Homepage of Amalthea: https://gitlab.com/tech.vindex/amalthea
 *
 * Copyright (C) 2018-2020 Eugene 'Vindex' Stulin
 *
 * link-editor and the Amalthea library are free software:
 * you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This libary is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module additional_functions;

import std.algorithm,
       std.process,
       std.string;

alias ssize_t = ptrdiff_t;

static string[][] localeStrings;
string[string] langconformity;
string currentLanguage = "en_US";


/*******************************************************************************
 * Current system language.
 */
string getSystemLanguage() {
    auto locale = environment.get("LANG", "en_US.UTF-8");
    return locale.split(".")[0];
}


/*******************************************************************************
 * Current application language by langlocal settings.
 */
string getCurrentLanguage() {
    return currentLanguage;
}


/*******************************************************************************
 * Initializes localization by a two-dimensional array of strings.
 * Titles of columns (first line - index 0) are locales
 * in Linux style without encoding (en_US, en_GB, eo, fr_CA, ru_RU, etc.),
 * first locale is en_US always
 *
 * Params:
 *      stringsWithLocalizations = two-dimensional array containing columns
 *                                 of strings in different languages,
 *      language = new current language (system language by default).
 *
 */
void initLocalization(in string[][] stringsWithLocalizations,
                      string language = "") {
    localeStrings = cast(string[][])stringsWithLocalizations;
    if (localeStrings[0][0] != "en_US") {
        throw new LocalizationsTableException(
            "Error: array in cell [0][0] must consist en_US");
    }
    if (language == "") language = getSystemLanguage;
    chooseLanguage(language);
}


/*******************************************************************************
 * This function allows to choose current locale for application
 */
void chooseLanguage(string language) {
    if (localeStrings.empty) return;
    foreach(i, locale; localeStrings[0]) {
        if (language == locale) {
            currentLanguage = language;
            size_t currentTableColumn = i;
            foreach(row; localeStrings) {
                if (row[currentTableColumn] == "") {
                    langconformity[row[0]] = row[0];
                    } else {
                        langconformity[row[0]] = row[currentTableColumn];
                    }
            }
            return;
        }
    }
    import std.stdio : stderr, writeln;
    stderr.writeln(
        "The localization " ~ language ~ " is not valid.\n" ~
        "The English language (en_US) is used.");
    currentLanguage = "en_US";
}


/*******************************************************************************
 * This function returns string corresponding to the English (en_US) version,
 * according to the current localization selected.
 */
string s_(string englishString) {
    return langconformity.get(englishString, englishString);
}
alias _s = s_;


class LocalizationsTableException : Exception {
    this(string msg) {
        super(msg);
    }
}


/*******************************************************************************
 * Function for extracting of value by a flag.
 * This function changes the passed arguments,
 * removing the flag with its value
 */
string extractOptionValue(ref string[] args,
                          string flag,
                          string alternativeFlag = "") {
    string value;
    ssize_t index1 = -1;

    cycle1: foreach(option; [flag, alternativeFlag]) {
        foreach(i, arg; args) {
            if (arg == option || arg.startsWith(option~"=")) {
                index1 = i;
                flag = option;
                break cycle1;
            }
        }
    }
    if (index1 == -1) return "";
    
    if (args[index1].startsWith(flag~"=")) {
        value = args[index1][(flag~"=").length .. $];
        auto tempArgs = args[0 .. index1];
        if (args.length > index1+1) tempArgs ~= args[index1+1 .. $];
        args = tempArgs;
    } else {
        if (args.length < index1+2) return "";
        value = args[index1+1];
        auto tempArgs = args[0 .. index1];
        if (args.length > index1+2) tempArgs ~= args[index1+2 .. $];
        args = tempArgs;
    }
    return value;
}
alias extractValueForFlag = extractOptionValue;


/*******************************************************************************
 * Function for extracting of values as array by a flag.
 * This function changes the passed arguments (args),
 * removing the flag with its values
 */
string[] extractOptionRange(ref string[] args,
                            string flag,
                            string altFlag = "") {
    string[] range;
    ssize_t index1 = args.countUntil(flag);
    ssize_t index2 = -1;

    if (index1 == -1) index1 = args.countUntil(altFlag);
    if (index1 == -1) return range;
    if (index1 != args.length-1 && !args[index1+1].startsWith("-")) {
        foreach(i, arg; args[index1+1 .. $]) {
            if (arg.startsWith("-")) {
                index2 = index1 + i;
                break;
            }
        }
        if (index2 != -1) {
            range = args[index1+1 .. index2+1];
            args = args[0 .. index1] ~ args[index2+1 .. $];
        } else {
            index2 = args.length - 1;
            range = args[index1+1 .. index2+1];
            args = args[0 .. index1];
        }
    }
    return range;
}
alias extractRangeForFlag = extractOptionRange;


/*******************************************************************************
 * Returns absolute path for any raw path
 */
string getAbsolutePath(string path) {
    import std.path : buildNormalizedPath, absolutePath;
    return buildNormalizedPath(absolutePath(path));
}
