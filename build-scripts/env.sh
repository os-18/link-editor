#!/bin/bash
APP=link-editor
PROJECT=$APP

ALL="changelog COPYING copyright Makefile README.md summary \
build-scripts/ help/ source/ testing/"

readonly VERSION=`cat source/version`

readonly BINPATH_BASEDIR=build/bin
BINPATH="${BINPATH_BASEDIR}/${APP}"


