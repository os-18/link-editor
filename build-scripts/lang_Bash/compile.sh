#!/bin/bash -e
source build-scripts/env.sh
SRCDIR=source/lang_Bash
SRC="${SRCDIR}/link-editor.sh"
MAIN_SRC_FILE="${SRCDIR}/link-editor.sh"

set -x

mkdir -p ${BINPATH_BASEDIR}

sed -i "s/^APP_VERSION.*/APP_VERSION=${VERSION}/g" "${MAIN_SRC_FILE}"

cp "${SRC}" "${BINPATH}"
chmod 755 ${BINPATH}

set +x
