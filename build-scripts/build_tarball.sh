#!/bin/bash -e
source build-scripts/env.sh

set -x

PKGNAME="${PROJECT}-${VERSION}"
TARNAME="${PKGNAME}.tar.xz"

mkdir -p build/${PKGNAME}
cp -p -r ${ALL} build/${PKGNAME}/
pushd build
tar --xz -cf ${TARNAME} ${PKGNAME}
popd

set +x

